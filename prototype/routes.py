import os
import secrets
from prototype import app, db, bcrypt, mail
from PIL import Image
from flask import render_template, url_for, flash, redirect, request, current_app, abort
from sqlalchemy import func, desc, asc
from prototype.forms import RegistrationForm, LoginForm, ProductForm, OrderForm, UpdateAccountForm, RequestResetForm, ResetPasswordForm, IngredientForm, AnnouncementsForm
from prototype.models import User, Product, Order, Ingredient, Announcements
from flask_login import login_user, current_user, logout_user, login_required
from functools import wraps
from flask_mail import Message
import stripe
from jinja2 import Template

#Prototype Code Adapted from Flask tutorials provided by Corey Schafer @ https://coreyms.com/


def admin_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        # Only the user logged into this account will be able to access admin functionality
        if current_user.email == "admin@cottagecakery.com":
            return f(*args, **kwargs)
        else:
            # The message that flashes when a non-admin user attempts to access admin functionality
            flash('You need to be an admin to view this page.', 'danger')
            return redirect(url_for('landing'))

    return wrap


@app.route("/")  # The route which first meets the user
@app.route("/landing")
def landing():
    return render_template('landing.html')


@app.route("/home")
def home():
    page = request.args.get('page', 1, type=int)
    products = Product.query.paginate(page=page, per_page=6)
    return render_template('home.html', products=products)


@app.route("/aboutus")
def aboutus():
    return render_template('aboutus.html')


@app.route("/faq")
def faq():
    return render_template('faq.html')


@app.route("/dashboard")
@login_required
@admin_required
def dashboard():
    order_count = db.session.query(Order).count()
    user_count = db.session.query(User).count()
    product_count = db.session.query(Product).count()
    ingredient_count = db.session.query(Ingredient).count()
    announcement_count = db.session.query(Announcements).count()
    popular_item = db.session.query(Order.item_name, func.count(Order.item_name).label('qty')).group_by(Order.item_name).order_by(desc('qty')).limit(1)
    unpopular_item = db.session.query(Order.item_name, func.count(Order.item_name).label('qty')).group_by(Order.item_name).order_by(asc('qty')).limit(1)
    popular_user = db.session.query(Order.user_name, func.count(Order.user_name).label('qty')).group_by(Order.user_name).order_by(desc('qty')).limit(1)
    return render_template('dashboard.html', popular_item=popular_item, unpopular_item=unpopular_item, popular_user=popular_user, ingredient_count=ingredient_count, announcement_count=announcement_count, order_count=order_count, user_count=user_count, product_count=product_count)

@app.route("/calculator")
@login_required
@admin_required
def calculator():
    ingredients = Ingredient.query.all()
    return render_template('calculator.html', ingredients=ingredients)

#Register and Log In


@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('landing'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(  # Passwords are hashed so as not to be stored in their raw form, 
            form.password.data).decode('utf-8')           # protecting them in the event of a breach  
        user = User(username=form.username.data,
                    email=form.email.data, password=hashed_password)
        # the user details are added to the session and committed - saving them to the database.
        db.session.add(user)
        db.session.commit()
        flash('Your account has sucessfully been created!', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:  # checks if the user is already logged in - if so they are redirected to the homepage
        return redirect(url_for('landing'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('landing'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)


@app.route("/logout")
def logout():
    # the login session is terminated and the user is logged out.
    logout_user()
    return redirect(url_for('login'))


@app.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    product = Product.query.all()
    user = User.query.all()
    order = db.session.query(Order.id, Product.name, Product.price, Order.date, Order.status)\
        .order_by(Order.date.desc())\
        .filter(Order.item_name == Product.name)\
        .filter(Order.user_name == User.username)\
        .filter(User.email == current_user.email)\
        .distinct()  # The order query is used to look for all orders in the name of the current user. Allowing the user to check the details of their orders.
    form = UpdateAccountForm()
    if form.validate_on_submit():
        # The UpdateAccountForm allows users to update their detials such as email and username - password can be updated in the login form.
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Your details have been updated', 'success')
        return redirect(url_for('account'))
    elif request.method == 'GET':
        # Used to prepopulate the form with the current user's details
        form.username.data = current_user.username
        form.email.data = current_user.email
    return render_template('account.html', title='Account', form=form, order=order, product=product, user=user)

#Handling new accounts (admin)


@app.route("/users", methods=['GET', 'POST'])
@login_required
@admin_required
def view_users():
    page = request.args.get('page', 1, type=int)
    users = User.query.paginate(
        per_page=10, page=page)  # pagination implemented
    return render_template('view_users.html', users=users)


@app.route("/view_one_user/<int:user_id>")
@login_required
@admin_required
def view_one_user(user_id):
    product = Product.query.all()
    user = User.query.get_or_404(user_id)
    order = db.session.query(Order.id, Product.name, Product.price, Order.date, Order.status)\
        .order_by(Order.date.desc())\
        .filter(Order.item_name == Product.name)\
        .filter(Order.user_name == User.username)\
        .filter(User.id == user_id)\
        .distinct()  # similar to the account page, this is used by the admin to view all the orders associated with a particular user.
    return render_template('view_one_user.html', title=user.username, user=user, order=order, product=product)


@app.route("/new_user", methods=['GET', 'POST'])
@login_required
@admin_required
def new_user():
    # Similar to the register, this allows the user to create new user accounts
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        user = User(username=form.username.data,
                    email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('The account has sucessfully been created!', 'success')
        return redirect(url_for('new_user'))
    return render_template('new_user.html', title='Create a new user', form=form)


@app.route("/users/<int:user_id>/delete", methods=['POST'])
@login_required
@admin_required
def delete_user(user_id):
    # this route is used to delete existing users.
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    flash('This user has been deleted', 'success')
    return redirect(url_for('view_users'))

#New Product


def save_picture(form_picture):  # this route is used to process pictures saved to the system
    # the names of the files are randomised to avoid name clashes
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(
        app.root_path, 'static/product_pics', picture_fn)

    # the images are saved to a specific size in order for them to be used in the application
    output_size = (400, 400)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


# This route is used to create and save details on new products
@app.route("/product/new", methods=['GET', 'POST'])
@login_required
@admin_required
def new_product():
    form = ProductForm()
    if form.validate_on_submit():
        if form.product_image_file.data:
            picture_file = save_picture(form.product_image_file.data)
            product_image_file = picture_file
        product = Product(name=form.name.data, price=form.price.data, price_API=form.price_API.data,
                          description=form.description.data, notes=form.notes.data, product_image_file=product_image_file)
        db.session.add(product)
        db.session.commit()
        flash('The product has been added', 'success')
        return redirect(url_for('new_product'))
    return render_template('new_product.html', title='New Product', form=form, legend='New Product')


# used to view specific products using product ID
@app.route("/product/<int:product_id>")
def product(product_id):
    product = Product.query.get_or_404(product_id)
    product_image_file = url_for(
        'static', filename='/product_pics/' + product.product_image_file)
    if current_user.is_authenticated:
        user = User.query.filter_by(id=current_user.id)
        session = stripe.checkout.Session.create(  # this session variable is used with stripe in order to pass the price details
            # saved in db to the stripe API allowing for transactions to take place.
            payment_method_types=['card'],
            line_items=[{
                'price': product.price_API,
                'quantity': 1,
            }],
            mode='payment',
            success_url=url_for('create_order', product_id=product.id,  # the details are passed to the create order method 
                                _external=True) + '?session_id={CHECKOUT_SESSION_ID}',#which adds the order details to the system.
            cancel_url=url_for('home', _external=True),
        )
        return render_template('product.html', title=product.name, product=product, user=user, product_image_file=product_image_file,
                               checkout_session_id=session['id'], checkout_public_key=app.config['STRIPE_PUBLIC_KEY'])
    return render_template('product.html', title=product.name, product=product, product_image_file=product_image_file)


# used to update the details of existing products
@app.route("/product/<int:product_id>/update", methods=['GET', 'POST'])
@login_required
@admin_required
def update_product(product_id):
    product = Product.query.get_or_404(product_id)
    form = ProductForm()
    if form.validate_on_submit():
        if form.product_image_file.data:
            picture_file = save_picture(form.product_image_file.data)
            product.product_image_file = picture_file
        product.name = form.name.data
        product.price = form.price.data
        product.price_API = form.price_API.data
        product.description = form.description.data
        product.notes = form.notes.data
        db.session.commit()
        flash('This product has been updated', 'success')
        return redirect(url_for('home', product_id=product.id))
    elif request.method == 'GET':
        form.name.data = product.name
        form.price.data = product.price
        form.price_API.data = product.price_API
        form.description.data = product.description
        form.notes.data = product.notes
    return render_template('new_product.html', title='Update Product', form=form, product=product, legend='Update Product')


# used to delete specific products
@app.route("/product/<int:product_id>/delete", methods=['POST'])
@login_required
@admin_required
def delete_product(product_id):
    product = Product.query.get_or_404(product_id)
    db.session.delete(product)
    db.session.commit()
    flash('This product has been deleted', 'success')
    return redirect(url_for('home'))


# this is the route used to create the details of the order within this system db
@app.route("/product/<int:product_id>/create_order", methods=['GET', 'POST'])
@login_required
def create_order(product_id):
    product = Product.query.get_or_404(product_id)
    order = Order(item_name=product.name,
                  user_name=current_user.username, status="Awaiting Confirmation")
    db.session.add(order)
    db.session.commit()
    msg = Message('Thank you for placing your order!',
                      sender='orders@cottagecakery.com',
                      recipients=[current_user.email])
    msg.body = f'''Thank you for your order, we appreciate you choosing the Cottage Cakery!
    
Please visit your dashboard to view any updates to your order!

Sincerely,
The Cottage Cakery'''
    mail.send(msg)
    flash('This order has been added', 'success')
    return redirect(url_for('thanks'))


# this is where the user is redirected following a successful transaction
@app.route("/thanks")
def thanks():
    return render_template('thanks.html')

#New Order


# view all orders in the system
@app.route("/view_orders", methods=['GET', 'POST'])
@login_required
@admin_required
def view_orders():
    page = request.args.get('page', 1, type=int)
    products = Product.query.all()
    users = User.query.all()
    orders = db.session.query(Order.id, Order.item_name, User.email, Order.user_name, Order.date, Order.status).distinct(
    ).order_by(Order.date.desc()).filter(User.username == Order.user_name).paginate(page=page, per_page=10)
    return render_template('view_orders.html', title='Orders', orders=orders, products=products, users=users)


@app.route("/view_one_order/<int:order_id>")
@login_required
@admin_required
def view_one_order(order_id):
    order = Order.query.get(order_id)
    return render_template('view_one_order.html', title="Edit Order", order=order)


# the admin may create a new order from scratch, query boxes allow for only existing users/products to be selected.
@app.route("/order/new", methods=['GET', 'POST'])
@login_required
@admin_required
def new_order():
    form = OrderForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.user_name.data.email).first()
        order = Order(item_name=form.item_name.data.name,
                      user_name=form.user_name.data.username, status=form.status.data)
        db.session.add(order)
        db.session.commit()
        flash('The order has been added', 'success')
        msg = Message('Your order has been created!',
                      sender='orders@cottagecakery.com',
                      recipients=[user.email])
        msg.body = f'''Please visit your dashboard to view details of your order!

Sincerely,
The Cottage Cakery'''
        mail.send(msg)
        return redirect(url_for('new_order'))
    return render_template('new_order.html', title='New Order', form=form, legend='New Order')


@app.route("/order/<int:order_id>/update", methods=['GET', 'POST'])
@login_required
@admin_required
def update_order(order_id):
    order = Order.query.get_or_404(order_id)
    form = OrderForm()
    if form.validate_on_submit():
        order.status = form.status.data
        user = User.query.filter_by(email=form.user_name.data.email).first()
        db.session.commit()  # After an order has been updated, an email is send to the user
        flash('This order has been updated', 'success')
        msg = Message('Your order has been updated!',
                      sender='orders@cottagecakery.com',
                      recipients=[user.email])
        msg.body = f'''Please visit your dashboard to view the update to your order!

Sincerely,
The Cottage Cakery'''
        mail.send(msg)
        return redirect(url_for('view_orders'))
    elif request.method == 'GET':
        form.item_name.data = order.item_name
        form.user_name.data = order.user_name
        form.status.data = order.status
    return render_template('new_order.html', title='Update Order', form=form, legend='Update Order')


# route is used to delete an order
@app.route("/order/<int:order_id>/delete", methods=['POST'])
@login_required
@admin_required
def delete_order(order_id):
    order = Order.query.get_or_404(order_id)
    db.session.delete(order)
    db.session.commit()
    flash('This order has been deleted', 'success')
    return redirect(url_for('view_orders'))

#Ingredients


@app.route("/ingredients", methods=['GET', 'POST'])  # used to view ingredients
@login_required
@admin_required
def view_ingredients():
    page = request.args.get('page', 1, type=int)
    ingredients = Ingredient.query.paginate(
        per_page=10, page=page)  # pagination
    return render_template('view_ingredients.html', ingredients=ingredients)


# used to view a single ingredient
@app.route("/view_one_ingredient/<int:ingredient_id>")
@login_required
@admin_required
def view_one_ingredient(ingredient_id):
    ingredient = Ingredient.query.get(ingredient_id)
    return render_template('view_one_ingredient.html', title="Edit ingredient", ingredient=ingredient)


# used to create a new ingredient
@app.route("/ingredient/new", methods=['GET', 'POST'])
@login_required
@admin_required
def new_ingredient():
    form = IngredientForm()
    if form.validate_on_submit():
        ingredient = Ingredient(name=form.name.data, price=form.price.data)
        db.session.add(ingredient)
        db.session.commit()
        flash('The ingredient has been added', 'success')
        return redirect(url_for('new_ingredient'))
    return render_template('new_ingredient.html', title='New ingredient', form=form, legend='New Ingredient')


# used to update an existing ingredient
@app.route("/ingredient/<int:ingredient_id>/update", methods=['GET', 'POST'])
@login_required
@admin_required
def update_ingredient(ingredient_id):
    ingredient = Ingredient.query.get_or_404(ingredient_id)
    form = IngredientForm()
    if form.validate_on_submit():
        ingredient.name = form.name.data
        ingredient.price = form.price.data
        db.session.commit()
        flash('This ingredient has been updated', 'success')
        return redirect(url_for('view_ingredients', ingredient_id=ingredient.id))
    elif request.method == 'GET':
        form.name.data = ingredient.name
        form.price.data = ingredient.price
    return render_template('new_ingredient.html', title='Update Ingredient', form=form, legend='Update Ingredient')


# used to delete an ingredient
@app.route("/ingredient/<int:ingredient_id>/delete", methods=['POST'])
@login_required
@admin_required
def delete_ingredient(ingredient_id):
    ingredient = Ingredient.query.get_or_404(ingredient_id)
    db.session.delete(ingredient)
    db.session.commit()
    flash('This ingredient has been deleted', 'success')
    return redirect(url_for('view_ingredients'))

#Announcements


@app.route("/announcements")  # used to view all announcements
def announcements():
    page = request.args.get('page', 1, type=int)
    announcements = Announcements.query.order_by(Announcements.date.desc()).paginate(per_page=5, page=page)
    return render_template('announcements.html', announcements=announcements)


# used to view a specific announcement
@app.route("/view_one_announcement/<int:announcement_id>")
@login_required
@admin_required
def view_one_announcements(announcement_id):
    announcement = Announcements.query.get(announcement_id)
    return render_template('view_one_announcement.html', title="Edit announcement", announcement=announcement)


# used to add a new announcement
@app.route("/announcements/new", methods=['GET', 'POST'])
@login_required
@admin_required
def new_announcement():
    form = AnnouncementsForm()
    if form.validate_on_submit():
        announcement = Announcements(
            title=form.title.data, content=form.content.data)
        db.session.add(announcement)
        db.session.commit()
        flash('The announcement has been added', 'success')
        return redirect(url_for('new_announcement'))
    return render_template('new_announcement.html', title='New announcement', form=form, legend='New Announcement')


# used to update existing annoucements
@app.route("/announcements/<int:announcement_id>/update", methods=['GET', 'POST'])
@login_required
@admin_required
def update_announcement(announcement_id):
    announcement = Announcements.query.get_or_404(announcement_id)
    form = AnnouncementsForm()
    if form.validate_on_submit():
        announcement.title = form.title.data
        announcement.content = form.content.data
        db.session.commit()
        flash('This announcement has been updated', 'success')
        return redirect(url_for('announcements', announcement_id=announcement.id))
    elif request.method == 'GET':
        form.title.data = announcement.title
        form.content.data = announcement.content
    return render_template('new_announcement.html', title='Update announcement', form=form, legend='Update Announcement')


# used to delete specific announcements
@app.route("/announcements/<int:announcement_id>/delete", methods=['POST'])
@login_required
@admin_required
def delete_announcement(announcement_id):
    announcement = Announcements.query.get_or_404(announcement_id)
    db.session.delete(announcement)
    db.session.commit()
    flash('This announcement has been deleted', 'success')
    return redirect(url_for('announcements'))

#Resetting password


# This route is used when the user requests a password reset.
def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Password Reset Request',
                  sender='passwordreset@cottagecakery.com',
                  recipients=[user.email])
    msg.body = f'''To reset your password, visit the following link:
{url_for('reset_token', token=token, _external=True)}
If you did not make this request then simply ignore this email and no changes will be made.
'''
    mail.send(msg)


# User inputs their email and the send_reset_email route sends an email with reset details.
@app.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('landing'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('An email has been sent with instructions to reset your password.', 'info')
        return redirect(url_for('login'))
    return render_template('reset_request.html', title='Reset Password', form=form)


# the user follows the link in their email that includes a token. This token allows them to reset the password for the requested email account.
@app.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('landing'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('That is an invalid or expired token', 'warning')
        return redirect(url_for('reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('Your password has been updated! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('reset_token.html', title='Reset Password', form=form)
