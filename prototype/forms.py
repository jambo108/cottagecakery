from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from flask_login import current_user
from wtforms import StringField, PasswordField, SubmitField, BooleanField, IntegerField, SelectField, FloatField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from prototype.models import User, Product
from wtforms.ext.sqlalchemy.fields import QuerySelectField

#Prototype Code Adapted from Flask tutorials provided by Corey Schafer @ https://coreyms.com/
#Registration Form


class RegistrationForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register!')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('This username is already in use.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('This email is already in use.')

#Login Form


class LoginForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')

#Update Account Details


class UpdateAccountForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    submit = SubmitField('Update!')

    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(username=username.data).first()
            if user:
                raise ValidationError('This username is already in use.')

    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email=email.data).first()
            if user:
                raise ValidationError('This email is already in use.')

#Reset User Password


class RequestResetForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset!')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError(
                'There is no associated account with that email, please create an account!')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')


def user_query():
    return User.query


def product_query():
    return Product.query


#Product Form


class ProductForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    price = IntegerField('Price', validators=[DataRequired()])
    price_API = StringField('Price API (from Stripe)',
                            validators=[DataRequired()])
    description = TextAreaField('Description', validators=[DataRequired()])
    notes = StringField('Notes', validators=[DataRequired()])
    product_image_file = FileField('Product Image', validators=[
                                   DataRequired(), FileAllowed(['jpg', 'png'])])
    submit = SubmitField('Submit!')

#Order Form


class OrderForm(FlaskForm):
    item_name = QuerySelectField('Item Name', query_factory=product_query,
                                 allow_blank=False, get_label='name', validators=[DataRequired()])
    user_name = QuerySelectField('Customer Email', query_factory=user_query,
                                 allow_blank=False, get_label='email', validators=[DataRequired()])
    status = SelectField('Status', choices=[('Awaiting Confirmation'), ('In Progress'), (
        'Ready for Collection'), ('Completed'), ('Cancelled')], validators=[DataRequired()])
    submit = SubmitField('Submit!')

#Ingredient Form


class IngredientForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    price = FloatField('Price per Unit', validators=[DataRequired()])
    submit = SubmitField('Submit!')

#Announcements Form


class AnnouncementsForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    content = TextAreaField('Announcement Content', validators=[DataRequired()])
    submit = SubmitField('Submit!')
