from prototype import db, login_manager, app
from flask_login import UserMixin
from flask_admin import Admin
from datetime import datetime
from flask_admin.contrib.sqla import ModelView
from sqlalchemy.orm import relationship
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

#Prototype Code Adapted from Flask tutorials provided by Corey Schafer @ https://coreyms.com/

#Log in Manager
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

#User Database Model
class User(db.Model, UserMixin):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(120), nullable=False, default='default.jpg')
    password = db.Column(db.String(60), nullable=False)

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"

#Product Database Model
class Product(db.Model):
    __tablename__ = 'product'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    price = db.Column(db.Integer, nullable=False)
    price_API = db.Column(db.String(30), nullable=False)
    description = db.Column(db.String(300), nullable=False)
    notes = db.Column(db.String(60), nullable=False)
    product_image_file = db.Column(db.String(120), nullable=False, default='defaultproduct.jpg')

    def __repr__(self):
            return f"Product('{self.name}', '{self.price}', '{self.description}', '{self.notes}', '{self.product_image_file}')"

#Order Database Model
class Order(db.Model):
    __tablename__ = 'order'

    id = db.Column(db.Integer, primary_key=True)
    item_name = db.Column(db.String(20), db.ForeignKey('product.id'), nullable=False)
    user_name = db.Column(db.String(20), db.ForeignKey('user.id'), nullable=False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    status = db.Column(db.String(25), nullable=False)

    users = db.relationship(User)
    products = db.relationship(Product)

#Ingredient Database Model
class Ingredient(db.Model):
    __tablename__ = 'ingredient'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    price = db.Column(db.Integer, nullable=False)

#Announcement Database Model
class Announcements(db.Model):
    __tablename__ = 'announcements'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(30), nullable=False)
    content = db.Column(db.String(300), nullable=False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


