import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_admin import Admin
from flask_mail import Mail
import stripe

#Prototype Code Adapted from Flask tutorials provided by Corey Schafer @ https://coreyms.com/

app = Flask(__name__)

#Database Config
app.config['SECRET_KEY'] = '58c255eb7abfd4ea07fd8008ee2d5db1'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'

app.config['STRIPE_PUBLIC_KEY'] = 'pk_test_51IWN0NHx9bV1H6YBpF8099yZKVFDmBEhvlskFR038nT1A2cg56i3Fwirbl48HdLwDiErC6X6BlogS4V0kofIpDL600Q305txFg'
app.config['STRIPE_SECRET_KEY'] = 'sk_test_51IWN0NHx9bV1H6YB0beXR6sgeVWtWJBiB0q5vk7gN5IOnO0vnzQKAgnB38kS1NQMfVebwF47nHOuZBPiSCZSAEvV00PBho1RQl'

stripe.api_key = app.config['STRIPE_SECRET_KEY']

#Database Components
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'
app.config['MAIL_SERVER'] = 'smtp.googlemail.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USERNAME'] = 'james.donaldson108@gmail.com'
app.config['MAIL_PASSWORD'] = 'CKfvvh4r'
#app.config['MAIL_USERNAME'] = os.environ.get('EMAIL_USER')
#app.config['MAIL_PASSWORD'] = os.environ.get('EMAIL_PASSWORD')
mail = Mail(app)

from prototype import routes
from prototype.errors.handlers import errors
app.register_blueprint(errors)
